#4. Design a script with selenium commands (not is necessary use some language programming)
#
#Open browser
#
#Go to Amazon page
#
#Find  the word “pantallas”
#
#Extract the number of result on the first screen
#
#
#
#5. Design a high level architecture script using POM
require "selenium-webdriver"

class HomePage

  def wait_element(type_selector, element, timeout: nil)
    timeout = timeout.nil? ? 30 : timeout
    begin
      wait = Selenium::WebDriver::Wait.new(timeout: timeout)
      wait.until { $driver.find_element(type_selector, element).displayed? }
      puts "Se encontró el elemento: #{element}"
    rescue
      raise "No se encontró el elemento: #{element}"
    end
  end

  def open_url(url)
    $driver.open(url.to_s)
  end

  def validate_page
    wait_element :id, "home_page", timeout: 60
  end

  def find(selector, element)
    $driver.find_element(selector, element)

  end

  def click_to(selector, element)
    find(selector, element).click
  end

  def send_text(selector, element, text)
    click_to(selector, element)
    $driver.find_element(selector, element).send_keys(text)
  end

end



home_page = HomePage.new
url_web = "https://www.amazon.com.mx"
home_page.open_url(url_web)
home_page.validate_page
home_page.send_text :id, 'twotabsearchtextbox','pantalls'
home_page.click_to :id, 'nav-search-submit-button'
result = home_page.find( :css, "a-section a-spacing-small a-spacing-top-small").text

puts "Resultados: #{result}"


