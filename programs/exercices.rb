
# Factorial de un número
def factorial(num)
  num.zero? ? 1 : num * factorial(num-1)
end

#b)From the following array ['a''b''c''d'], validate if it contains the following values: 'a' 'b' 'f'. Return a Boolean depending on the result
def validate_array
  puts "From the following array ['a''b''c''d'], validate if it contains the following values: 'a' 'b' 'f'. Return a Boolean depending on the result"
  array_base = %w[a b c d]
  array_to_validate = %w[a b f]
  array_to_validate.each do |char|
    array_base.include?(char) ? puts("letra #{char} --> true") : puts("letra #{char} --> false")
  end
end

# c)Sum all the elements of an array: [1,34,56,72,123]
def sum_elements
  puts "Sum all the elements of an array: [1,34,56,72,123]"
  elements = [1,34,56,72,123]
  suma = 0
  elements.each { |num| suma += num }
  puts "El valor de la suma de todos los elementos es: #{suma}"
end

#Write a program to find the number of days in a month
def days(year, month)
  @months = {
    1 => "enero", 2 => "febrero", 3 => "marzo", 4 => "abril", 5 => "mayo",
    6 => "junio", 7 => "julio", 8 => "agosto", 9 => "septiembre", 10 => "octubre", 11 => "noviembre", 12 => "diciembre"
  }
  raise "El número de año tiene que ser Entero" unless year.is_a? Integer
  raise "El número de mes tiene que ser Entero" unless month.is_a? Integer
  raise "El número de mes es invalido" unless @months.include? month
  days_month = Date.new(year, month, -1).day
  puts "El mes #{@months[month]} del año #{year} tiene #{days_month}"
end

# Write a program to find the number of days in a month
def without_module(num1, num2)
  return unless num1 > num2
  resta_num = num1 -= num2
  return resta_num if num1 < num2
  without_module(num1, num2)
end