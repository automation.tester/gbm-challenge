Feature: Purchase product
-As user I should be able to purchase a product

  Scenario: Purchase and select product
    Given I want to purchase a product
    Then I login on the app
    When I go to the categories
    And I select the product
    Then I should see the product detail
    And I add it to the cart
    Then I should se the purchase detail
    And I fill the purchase information.
    Then I fiinally I purchase the product.